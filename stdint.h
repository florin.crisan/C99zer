#ifndef C99ZER_STDINT_H
#define C99ZER_STDINT_H

/*
    This file is part of C99zer.
    Copyright © 2017 Florin Crișan
    Distributed under the MIT license <https://opensource.org/licenses/MIT>
*/

#include <stddef.h>
#include <limits.h>

/* size_t is a standard C89 type, but SIZE_MAX is only defined in C99. */

#ifndef SIZE_MAX
#   if defined(ULLONG_MAX) && sizeof(size_t) == sizeof(unsigned long long)
#       define SIZE_MAX ULLONG_MAX
#   elif sizeof(size_t) == sizeof(unsigned long)
#       define SIZE_MAX ULONG_MAX
#   elif sizeof(size_t) == sizeof(unsigned int)
#       define SIZE_MAX UINT_MAX
#   else
#       error Do not know how big size_t is.
#   endif
#endif /*!defined(SIZE_MAX)*/

/* ptrdiff_t is a standard C89 type, but PTRDIFF_MIN/MAX are only defined in C99. */

#ifndef PTRDIFF_MAX
#   if defined(LLONG_MAX) && sizeof(ptrdiff_t) == sizeof(long long)
#       define PTRDIFF_MIN LLONG_MIN
#       define PTRDIFF_MAX LLONG_MAX
#   elif sizeof(ptrdiff_t) == sizeof(long)
#       define PTRDIFF_MIN LONG_MIN
#       define PTRDIFF_MAX LONG_MAX
#   elif sizeof(ptrdiff_t) == sizeof(int)
#       define PTRDIFF_MIN INT_MIN
#       define PTRDIFF_MAX INT_MAX
#   else
#       error Do not know how big size_t is.
#   endif
#endif /*!defined(SIZE_MAX)*/


/* intmax_t is the largest integer type available. It could be bigger in theory than long long or long, but we're just going for the standard types. */

#ifdef LLONG_MAX

    typedef long long intmax_t;

    #define INTMAX_MIN LLONG_MIN
    #define INTMAX_MAX LLONG_MAX

    #define INTMAX_C(x) ((intmax_t)(x))

    #define PRIdMAX "lld"
    #define PRIiMAX "lli"
    #define PRIuMAX "llu"
    #define PRIoMAX "llo"
    #define PRIxMAX "llx"
    #define PRIXMAX "llX"

#else /*ifdef LLONG_MAX*/

    #ifndef LONG_MAX
    #   error LONG_MAX is not defined
    #endif /*#ifndef LONG_MAX*/

    typedef long intmax_t;

    #define INTMAX_MIN LONG_MIN
    #define INTMAX_MAX LONG_MAX

    #define INTMAX_C(x) ((intmax_t)(x))

    #define PRIdMAX "ld"
    #define PRIiMAX "li"
    #define PRIuMAX "lu"
    #define PRIoMAX "lo"
    #define PRIxMAX "lx"
    #define PRIXMAX "lX"

#endif /*#ifdef LLONG_MAX*/


/* intptr_t, uintptr_t */

#if !defined(INTPTR_MIN) || !defined(INTPTR_MAX) || !defined(UINTPTR_MAX)

    #if sizeof(intptr_t) == sizeof(int)

        typedef int intptr_t;
        typedef unsigned int uintptr_t;

        #define INTPTR_MIN INT_MIN
        #define INTPTR_MAX INT_MAX
        #define UINTPTR_MAX UINT_MAX

        #define PRIdPTR "d"
        #define PRIiPTR "i"
        #define PRIuPTR "u"
        #define PRIoPTR "o"
        #define PRIxPTR "x"
        #define PRIXPTR "X"

    #elif sizeof(intptr_t) == sizeof(long int)

        typedef long int intptr_t;
        typedef unsigned long int uintptr_t;

        #define INTPTR_MIN LONG_MIN
        #define INTPTR_MAX LONG_MAX
        #define UINTPTR_MAX ULONG_MAX

        #define PRIdPTR "ld"
        #define PRIiPTR "li"
        #define PRIuPTR "lu"
        #define PRIoPTR "lo"
        #define PRIxPTR "lx"
        #define PRIXPTR "lX"

    #elif defined(LLONG_MAX) && sizeof(intptr_t) == sizeof(long long int)

        typedef long long int intptr_t;
        typedef unsigned long long int uintptr_t;

        #define INTPTR_MIN LLONG_MIN
        #define INTPTR_MAX LLONG_MAX
        #define UINTPTR_MAX ULLONG_MAX

        #define PRIdPTR "lld"
        #define PRIiPTR "lli"
        #define PRIuPTR "llu"
        #define PRIoPTR "llo"
        #define PRIxPTR "llx"
        #define PRIXPTR "llX"

    #else

        #error Do not know to store a pointer as an integer.

    #endif

#endif

/* uintmax_t */

#ifdef ULLONG_MAX

    typedef unsigned long long uintmax_t;

    #define UINTMAX_MAX ULLONG_MAX

    #define UINTMAX_C(x) ((uintmax_t)(x))

    #define PRIdMAX "lld"
    #define PRIiMAX "lli"
    #define PRIuMAX "llu"
    #define PRIoMAX "llo"
    #define PRIxMAX "llx"
    #define PRIXMAX "llX"

#else /*ifdef ULLONG_MAX*/

    #ifndef ULONG_MAX
    # error ULONG_MAX is not defined
    #endif /*#ifndef ULONG_MAX*/

    typedef unsigned long uintmax_t;

    #define UINTMAX_MAX ULONG_MAX

    #define UINTMAX_C(x) ((uintmax_t)(x))

    #define PRIdMAX "ld"
    #define PRIiMAX "li"
    #define PRIuMAX "lu"
    #define PRIoMAX "lo"
    #define PRIxMAX "lx"
    #define PRIXMAX "lX"

#endif /*#ifdef LLONG_MAX*/


/* Fixed-width integers. Should only be defined on 8-bit, 2's complement machines. */

#if (CHAR_BIT == 8) && ((signed char)0xFF == (signed char)-1)

    /* int8_t */

    #if SCHAR_MIN == -128 && SCHAR_MAX == 127

        typedef signed char int8_t;

        #define INT8_MIN SCHAR_MIN
        #define INT8_MAX SCHAR_MAX

        #define INT8_C(x) ((int8_t)x)

        #define PRId8 "hd"
        #define PRIi8 "hi"
        #define PRIu8 "hu"
        #define PRIo8 "ho"
        #define PRIx8 "hx"
        #define PRIX8 "hX"

    #endif

    /* uint8_t */

    #if UCHAR_MAX == 255u

        typedef unsigned char uint8_t;

        #define UINT8_MAX UCHAR_MAX

        #define UINT8_C(x) ((uint8_t)x ## u)

        #define PRId8 "hd"
        #define PRIi8 "hi"
        #define PRIu8 "hu"
        #define PRIo8 "ho"
        #define PRIx8 "hx"
        #define PRIX8 "hX"

    #endif

    /* int16_t */

    #if SCHAR_MIN == -32768 && SCHAR_MAX == 32767

        typedef signed char int16_t;

        #define INT16_MIN SCHAR_MIN
        #define INT16_MAX SCHAR_MAX

        #define INT16_C(x) ((int16_t)x)

        #define PRId16 "hd"
        #define PRIi16 "hi"
        #define PRIu16 "hu"
        #define PRIo16 "ho"
        #define PRIx16 "hx"
        #define PRIX16 "hX"

    #elif SHRT_MIN == -32768 && SHRT_MAX == 32767

        typedef signed short int int16_t;

        #define INT16_MIN SHRT_MIN
        #define INT16_MAX SHRT_MAX

        #define INT16_C(x) ((int16_t)x)

        #define PRId16 "hd"
        #define PRIi16 "hi"
        #define PRIu16 "hu"
        #define PRIo16 "ho"
        #define PRIx16 "hx"
        #define PRIX16 "hX"

    #elif INT_MIN == -32768 && INT_MAX == 32767

        typedef signed int int16_t;

        #define INT16_MIN INT_MIN
        #define INT16_MAX INT_MAX

        #define INT16_C(x) ((int16_t)x)

        #define PRId16 "d"
        #define PRIi16 "i"
        #define PRIu16 "u"
        #define PRIo16 "o"
        #define PRIx16 "x"
        #define PRIX16 "X"

    #elif LONG_MIN == -32768 && LONG_MAX == 32767

        typedef signed long int int16_t;

        #define INT16_MIN LONG_MIN
        #define INT16_MAX LONG_MAX

        #define INT16_C(x) ((int16_t)x ## L)

        #define PRId16 "ld"
        #define PRIi16 "li"
        #define PRIu16 "lu"
        #define PRIo16 "lo"
        #define PRIx16 "lx"
        #define PRIX16 "lX"

    #elif defined(LLONG_MIN) && LLONG_MIN == -32768 && defined(LLONG_MAX) && LLONG_MAX == 32767

        typedef signed long long int int16_t;

        #define INT16_MIN LLONG_MIN
        #define INT16_MAX LLONG_MAX

        #define INT16_C(x) ((int16_t)x ## LL)

        #define PRId16 "lld"
        #define PRIi16 "lli"
        #define PRIu16 "llu"
        #define PRIo16 "llo"
        #define PRIx16 "llx"
        #define PRIX16 "llX"

    #endif

    /* uint16_t */

    #if UCHAR_MAX == 65535u

        typedef unsigned char uint16_t;

        #define UINT16_MAX UCHAR_MAX

        #define UINT16_C(x) ((uint16_t)x ## U)

        #define PRId16 "hd"
        #define PRIi16 "hi"
        #define PRIu16 "hu"
        #define PRIo16 "ho"
        #define PRIx16 "hx"
        #define PRIX16 "hX"

    #elif USHRT_MAX == 65535u

    typedef unsigned short int uint16_t;

        #define UINT16_MAX USHRT_MAX

        #define UINT16_C(x) ((uint16_t)x ## U)

        #define PRId16 "hd"
        #define PRIi16 "hi"
        #define PRIu16 "hu"
        #define PRIo16 "ho"
        #define PRIx16 "hx"
        #define PRIX16 "hX"

    #elif UINT_MAX == 65535u

        typedef unsigned int uint16_t;

        #define UINT16_MAX UINT_MAX

        #define UINT16_C(x) ((uint16_t)x ## U)

        #define PRId16 "d"
        #define PRIi16 "i"
        #define PRIu16 "u"
        #define PRIo16 "o"
        #define PRIx16 "x"
        #define PRIX16 "X"

    #elif ULONG_MAX == 65535u

        typedef unsigned long int uint16_t;

        #define UINT16_MAX ULONG_MAX

        #define UINT16_C(x) ((uint16_t)x ## LU)

        #define PRId16 "ld"
        #define PRIi16 "li"
        #define PRIu16 "lu"
        #define PRIo16 "lo"
        #define PRIx16 "lx"
        #define PRIX16 "lX"

    #elif defined(ULLONG_MAX) && ULLONG_MAX == 65535u

        typedef unsigned long long int uint16_t;

        #define UINT16_MAX ULLONG_MAX

        #define UINT16_C(x) ((uint16_t)x ## LLU)

        #define PRId16 "lld"
        #define PRIi16 "lli"
        #define PRIu16 "llu"
        #define PRIo16 "llo"
        #define PRIx16 "llx"
        #define PRIX16 "llX"

    #endif

    /* int32_t */

    #if SCHAR_MIN == -2147483648 && SCHAR_MAX == 2147483647

        typedef signed char int32_t;

        #define INT32_MIN SCHAR_MIN
        #define INT32_MAX SCHAR_MAX

        #define INT32_C(x) ((int32_t)x)

        #define PRId32 "hd"
        #define PRIi32 "hi"
        #define PRIu32 "hu"
        #define PRIo32 "ho"
        #define PRIx32 "hx"
        #define PRIX32 "hX"

    #elif SHRT_MIN == -2147483648 && SHRT_MAX == 2147483647

        typedef signed short int int32_t;

        #define INT32_MIN SHRT_MIN
        #define INT32_MAX SHRT_MAX

        #define INT32_C(x) ((int32_t)x)

        #define PRId32 "hd"
        #define PRIi32 "hi"
        #define PRIu32 "hu"
        #define PRIo32 "ho"
        #define PRIx32 "hx"
        #define PRIX32 "hX"

    #elif INT_MIN == -2147483648 && INT_MAX == 2147483647

        typedef signed int int32_t;

        #define INT32_MIN INT_MIN
        #define INT32_MAX INT_MAX

        #define INT32_C(x) ((int32_t)x)

        #define PRId32 "d"
        #define PRIi32 "i"
        #define PRIu32 "u"
        #define PRIo32 "o"
        #define PRIx32 "x"
        #define PRIX32 "X"

    #elif LONG_MIN == -2147483648 && LONG_MAX == 2147483647

        typedef signed long int int32_t;

        #define INT32_MIN LONG_MIN
        #define INT32_MAX LONG_MAX

        #define INT32_C(x) ((int32_t)x ## L)

        #define PRId32 "ld"
        #define PRIi32 "li"
        #define PRIu32 "lu"
        #define PRIo32 "lo"
        #define PRIx32 "lx"
        #define PRIX32 "lX"

    #elif defined(LLONG_MIN) && LLONG_MIN == -2147483648 && defined(LLONG_MAX) && LLONG_MAX == 2147483647

        typedef signed long long int int32_t;

        #define INT32_MIN LLONG_MIN
        #define INT32_MAX LLONG_MAX

        #define INT32_C(x) ((int32_t)x ## LL)

        #define PRId32 "lld"
        #define PRIi32 "lli"
        #define PRIu32 "llu"
        #define PRIo32 "llo"
        #define PRIx32 "llx"
        #define PRIX32 "llX"

    #endif

    /* uint32_t */

    #if UCHAR_MAX == 4294967295u

        typedef unsigned char uint32_t;

        #define UINT32_MAX UCHAR_MAX

        #define UINT32_C(x) ((uint32_t)x ## U)

        #define PRId32 "hd"
        #define PRIi32 "hi"
        #define PRIu32 "hu"
        #define PRIo32 "ho"
        #define PRIx32 "hx"
        #define PRIX32 "hX"

    #elif USHRT_MAX == 4294967295u

        typedef unsigned short int uint32_t;

        #define UINT32_MAX USHRT_MAX

        #define UINT32_C(x) ((uint32_t)x ## U)

        #define PRId32 "hd"
        #define PRIi32 "hi"
        #define PRIu32 "hu"
        #define PRIo32 "ho"
        #define PRIx32 "hx"
        #define PRIX32 "hX"

    #elif UINT_MAX == 4294967295u

        typedef unsigned int uint32_t;

        #define UINT32_MAX UINT_MAX

        #define UINT32_C(x) ((uint32_t)x ## U)

        #define PRId32 "d"
        #define PRIi32 "i"
        #define PRIu32 "u"
        #define PRIo32 "o"
        #define PRIx32 "x"
        #define PRIX32 "X"

    #elif ULONG_MAX == 4294967295u

        typedef unsigned long int uint32_t;

        #define UINT32_MAX ULONG_MAX

        #define UINT32_C(x) ((uint32_t)x ## LU)

        #define PRId32 "ld"
        #define PRIi32 "li"
        #define PRIu32 "lu"
        #define PRIo32 "lo"
        #define PRIx32 "lx"
        #define PRIX32 "lX"

    #elif defined(ULLONG_MAX) && ULLONG_MAX == 4294967295u

        typedef unsigned long long int uint32_t;

        #define UINT32_MAX ULLONG_MAX

        #define UINT32_C(x) ((uint32_t)x ## LLU)

        #define PRId32 "lld"
        #define PRIi32 "lli"
        #define PRIu32 "llu"
        #define PRIo32 "llo"
        #define PRIx32 "llx"
        #define PRIX32 "llX"

    #endif

    /* int64_t */

    #if SCHAR_MIN == -9223372036854775808 && SCHAR_MAX == 9223372036854775807

        typedef signed char int64_t;

        #define INT64_MIN SCHAR_MIN
        #define INT64_MAX SCHAR_MAX

        #define INT64_C(x) ((int64_t)x)

        #define PRId64 "hd"
        #define PRIi64 "hi"
        #define PRIu64 "hu"
        #define PRIo64 "ho"
        #define PRIx64 "hx"
        #define PRIX64 "hX"

    #elif SHRT_MIN == -9223372036854775808 && SHRT_MAX == 9223372036854775807

        typedef signed short int int64_t;

        #define INT64_MIN SHRT_MIN
        #define INT64_MAX SHRT_MAX

        #define INT64_C(x) ((int64_t)x)

        #define PRId64 "hd"
        #define PRIi64 "hi"
        #define PRIu64 "hu"
        #define PRIo64 "ho"
        #define PRIx64 "hx"
        #define PRIX64 "hX"

    #elif INT_MIN == -9223372036854775808 && INT_MAX == 9223372036854775807

        typedef signed int int64_t;

        #define INT64_MIN INT_MIN
        #define INT64_MAX INT_MAX

        #define INT64_C(x) ((int64_t)x)

        #define PRId64 "d"
        #define PRIi64 "i"
        #define PRIu64 "u"
        #define PRIo64 "o"
        #define PRIx64 "x"
        #define PRIX64 "X"

    #elif LONG_MIN == -9223372036854775808 && LONG_MAX == 9223372036854775807

        typedef signed long int int64_t;

        #define INT64_MIN LONG_MIN
        #define INT64_MAX LONG_MAX

        #define INT64_C(x) ((int64_t)x ## L)

        #define PRId64 "ld"
        #define PRIi64 "li"
        #define PRIu64 "lu"
        #define PRIo64 "lo"
        #define PRIx64 "lx"
        #define PRIX64 "lX"

    #elif defined(LLONG_MIN) && LLONG_MIN == -9223372036854775808 && defined(LLONG_MAX) && LLONG_MAX == 9223372036854775807

        typedef signed long long int int64_t;

        #define INT64_MIN LLONG_MIN
        #define INT64_MAX LLONG_MAX

        #define INT64_C(x) ((int64_t)x ## LL)

        #define PRId64 "lld"
        #define PRIi64 "lli"
        #define PRIu64 "llu"
        #define PRIo64 "llo"
        #define PRIx64 "llx"
        #define PRIX64 "llX"

    #endif

    /* uint64_t */

    #if UCHAR_MAX == 18446744073709551615u

        typedef unsigned char uint64_t;

        #define UINT64_MAX UCHAR_MAX

        #define UINT64_C(x) ((uint64_t)x ## U)

        #define PRId64 "hd"
        #define PRIi64 "hi"
        #define PRIu64 "hu"
        #define PRIo64 "ho"
        #define PRIx64 "hx"
        #define PRIX64 "hX"

    #elif USHRT_MAX == 18446744073709551615u

        typedef unsigned short int uint64_t;

        #define UINT64_MAX USHRT_MAX

        #define UINT64_C(x) ((uint64_t)x ## U)

        #define PRId64 "hd"
        #define PRIi64 "hi"
        #define PRIu64 "hu"
        #define PRIo64 "ho"
        #define PRIx64 "hx"
        #define PRIX64 "hX"

    #elif UINT_MAX == 18446744073709551615u

        typedef unsigned int uint64_t;

        #define UINT64_MAX UINT_MAX

        #define UINT64_C(x) ((uint64_t)x ## U)

        #define PRId64 "d"
        #define PRIi64 "i"
        #define PRIu64 "u"
        #define PRIo64 "o"
        #define PRIx64 "x"
        #define PRIX64 "X"

    #elif ULONG_MAX == 18446744073709551615u

        typedef unsigned long int uint64_t;

        #define UINT64_MAX ULONG_MAX

        #define UINT64_C(x) ((uint64_t)x ## LU)

        #define PRId64 "ld"
        #define PRIi64 "li"
        #define PRIu64 "lu"
        #define PRIo64 "lo"
        #define PRIx64 "lx"
        #define PRIX64 "lX"

    #elif defined(ULLONG_MAX) && ULLONG_MAX == 18446744073709551615u

        typedef unsigned long long int uint64_t;

        #define UINT64_MAX ULLONG_MAX

        #define UINT64_C(x) ((uint64_t)x ## LLU)

        #define PRId64 "lld"
        #define PRIi64 "lli"
        #define PRIu64 "llu"
        #define PRIo64 "llo"
        #define PRIx64 "llx"
        #define PRIX64 "llX"

    #endif

#endif /*#if CHAR_BIT == 8*/


/* Integers with at least the whole range of the fixed-width ones. */

/* int_least8_t */

#if SCHAR_MIN <= -128 && SCHAR_MAX >= 127

    typedef signed char int_least8_t;

    #define INT_LEAST8_MIN SCHAR_MIN
    #define INT_LEAST8_MAX SCHAR_MAX

    #define INT_LEAST8_C(x) ((int_least8_t)x)

    #define PRIdLEAST8 "hd"
    #define PRIiLEAST8 "hi"
    #define PRIuLEAST8 "hu"
    #define PRIoLEAST8 "ho"
    #define PRIxLEAST8 "hx"
    #define PRIXLEAST8 "hX"

#elif SHRT_MIN <= -128 && SHRT_MAX >= 127

    typedef signed short int int_least8_t;

    #define INT_LEAST8_MIN SHRT_MIN
    #define INT_LEAST8_MAX SHRT_MAX

    #define INT_LEAST8_C(x) ((int_least8_t)x)

    #define PRIdLEAST8 "hd"
    #define PRIiLEAST8 "hi"
    #define PRIuLEAST8 "hu"
    #define PRIoLEAST8 "ho"
    #define PRIxLEAST8 "hx"
    #define PRIXLEAST8 "hX"

#elif INT_MIN <= -128 && INT_MAX >= 127

    typedef signed int int_least8_t;

    #define INT_LEAST8_MIN INT_MIN
    #define INT_LEAST8_MAX INT_MAX

    #define INT_LEAST8_C(x) ((int_least8_t)x)

    #define PRIdLEAST8 "d"
    #define PRIiLEAST8 "i"
    #define PRIuLEAST8 "u"
    #define PRIoLEAST8 "o"
    #define PRIxLEAST8 "x"
    #define PRIXLEAST8 "X"

#elif LONG_MIN <= -128 && LONG_MAX >= 127

    typedef signed long int int_least8_t;

    #define INT_LEAST8_MIN LONG_MIN
    #define INT_LEAST8_MAX LONG_MAX

    #define INT_LEAST8_C(x) ((int_least8_t)x ## L)

    #define PRIdLEAST8 "ld"
    #define PRIiLEAST8 "li"
    #define PRIuLEAST8 "lu"
    #define PRIoLEAST8 "lo"
    #define PRIxLEAST8 "lx"
    #define PRIXLEAST8 "lX"

#elif defined(LLONG_MIN) && defined(LLONG_MAX) && LLONG_MIN <= -128 && LLONG_MAX >= 127

    typedef signed long long int int_least8_t;

    #define INT_LEAST8_MIN LLONG_MIN
    #define INT_LEAST8_MAX LLONG_MAX

    #define INT_LEAST8_C(x) ((int_least8_t)x ## LL)

    #define PRIdLEAST8 "lld"
    #define PRIiLEAST8 "lli"
    #define PRIuLEAST8 "llu"
    #define PRIoLEAST8 "llo"
    #define PRIxLEAST8 "llx"
    #define PRIXLEAST8 "llX"

#endif

/* uint_least8_t */

#if UCHAR_MAX >= 255u

    typedef unsigned char uint_least8_t;

    #define UINT_LEAST8_MAX UCHAR_MAX

    #define UINT_LEAST8_C(x) ((uint_least8_t)x ## U)

    #define PRIdLEAST8 "hd"
    #define PRIiLEAST8 "hi"
    #define PRIuLEAST8 "hu"
    #define PRIoLEAST8 "ho"
    #define PRIxLEAST8 "hx"
    #define PRIXLEAST8 "hX"

#elif USHRT_MAX >= 255u

    typedef unsigned short int uint_least8_t;

    #define UINT_LEAST8_MAX USHRT_MAX

    #define UINT_LEAST8_C(x) ((uint_least8_t)x ## U)

    #define PRIdLEAST8 "hd"
    #define PRIiLEAST8 "hi"
    #define PRIuLEAST8 "hu"
    #define PRIoLEAST8 "ho"
    #define PRIxLEAST8 "hx"
    #define PRIXLEAST8 "hX"

#elif UINT_MAX >= 255u

    typedef unsigned int uint_least8_t;

    #define UINT_LEAST8_MAX UINT_MAX

    #define UINT_LEAST8_C(x) ((uint_least8_t)x ## U)

    #define PRIdLEAST8 "d"
    #define PRIiLEAST8 "i"
    #define PRIuLEAST8 "u"
    #define PRIoLEAST8 "o"
    #define PRIxLEAST8 "x"
    #define PRIXLEAST8 "X"

#elif LONG_MAX >= 255u

    typedef unsigned long int uint_least8_t;

    #define UINT_LEAST8_MAX LONG_MAX

    #define UINT_LEAST8_C(x) ((uint_least8_t)x ## LU)

    #define PRIdLEAST8 "ld"
    #define PRIiLEAST8 "li"
    #define PRIuLEAST8 "lu"
    #define PRIoLEAST8 "lo"
    #define PRIxLEAST8 "lx"
    #define PRIXLEAST8 "lX"

#elif defined(LLONG_MAX) && LLONG_MAX >= 255u

    typedef unsigned long long int uint_least8_t;

    #define UINT_LEAST8_MAX LLONG_MAX

    #define UINT_LEAST8_C(x) ((uint_least8_t)x ## LLU)

    #define PRIdLEAST8 "lld"
    #define PRIiLEAST8 "lli"
    #define PRIuLEAST8 "llu"
    #define PRIoLEAST8 "llo"
    #define PRIxLEAST8 "llx"
    #define PRIXLEAST8 "llX"

#endif

/* int_least16_t */

#if SCHAR_MIN <= -32768 && SCHAR_MAX >= 32767

    typedef signed char int_least16_t;

    #define INT_LEAST16_MIN SCHAR_MIN
    #define INT_LEAST16_MAX SCHAR_MAX

    #define INT_LEAST16_C(x) ((int_least16_t)x)

    #define PRIdLEAST16 "hd"
    #define PRIiLEAST16 "hi"
    #define PRIuLEAST16 "hu"
    #define PRIoLEAST16 "ho"
    #define PRIxLEAST16 "hx"
    #define PRIXLEAST16 "hX"

#elif SHRT_MIN <= -32768 && SHRT_MAX <= 32767

    typedef signed short int int_least16_t;

    #define INT_LEAST16_MIN SHRT_MIN
    #define INT_LEAST16_MAX SHRT_MAX

    #define INT_LEAST16_C(x) ((int_least16_t)x)

    #define PRIdLEAST16 "hd"
    #define PRIiLEAST16 "hi"
    #define PRIuLEAST16 "hu"
    #define PRIoLEAST16 "ho"
    #define PRIxLEAST16 "hx"
    #define PRIXLEAST16 "hX"

#elif INT_MIN <= -32768 && INT_MAX >= 32767

    typedef signed int int_least16_t;

    #define INT_LEAST16_MIN INT_MIN
    #define INT_LEAST16_MAX INT_MAX

    #define INT_LEAST16_C(x) ((int_least16_t)x)

    #define PRIdLEAST16 "d"
    #define PRIiLEAST16 "i"
    #define PRIuLEAST16 "u"
    #define PRIoLEAST16 "o"
    #define PRIxLEAST16 "x"
    #define PRIXLEAST16 "X"

#elif LONG_MIN <= -32768 && LONG_MAX >= 32767

    typedef signed long int int_least16_t;

    #define INT_LEAST16_MIN LONG_MIN
    #define INT_LEAST16_MAX LONG_MAX

    #define INT_LEAST16_C(x) ((int_least16_t)x ## L)

    #define PRIdLEAST16 "ld"
    #define PRIiLEAST16 "li"
    #define PRIuLEAST16 "lu"
    #define PRIoLEAST16 "lo"
    #define PRIxLEAST16 "lx"
    #define PRIXLEAST16 "lX"

#elif defined(LLONG_MIN) && LLONG_MIN <= -32768 && defined(LLONG_MAX) && LLONG_MAX >= 32767

    typedef signed long long int int_least16_t;

    #define INT_LEAST16_MIN LLONG_MIN
    #define INT_LEAST16_MAX LLONG_MAX

    #define INT_LEAST16_C(x) ((int_least16_t)x ## LL)

    #define PRIdLEAST16 "lld"
    #define PRIiLEAST16 "lli"
    #define PRIuLEAST16 "llu"
    #define PRIoLEAST16 "llo"
    #define PRIxLEAST16 "llx"
    #define PRIXLEAST16 "llX"

#endif

/* uint_least16_t */

#if UCHAR_MAX >= 65535u

    typedef unsigned char uint_least16_t;

    #define UINT_LEAST16_MAX UCHAR_MAX

    #define UINT_LEAST16_C(x) ((uint_least16_t)x ## U)

    #define PRIdLEAST16 "hd"
    #define PRIiLEAST16 "hi"
    #define PRIuLEAST16 "hu"
    #define PRIoLEAST16 "ho"
    #define PRIxLEAST16 "hx"
    #define PRIXLEAST16 "hX"

#elif USHRT_MAX >= 65535u

typedef unsigned short int uint_least16_t;

    #define UINT_LEAST16_MAX USHRT_MAX

    #define UINT_LEAST16_C(x) ((uint_least16_t)x ## U)

    #define PRIdLEAST16 "hd"
    #define PRIiLEAST16 "hi"
    #define PRIuLEAST16 "hu"
    #define PRIoLEAST16 "ho"
    #define PRIxLEAST16 "hx"
    #define PRIXLEAST16 "hX"

#elif UINT_MAX >= 65535u

    typedef unsigned int uint_least16_t;

    #define UINT_LEAST16_MAX UINT_MAX

    #define UINT_LEAST16_C(x) ((uint_least16_t)x ## U)

    #define PRIdLEAST16 "d"
    #define PRIiLEAST16 "i"
    #define PRIuLEAST16 "u"
    #define PRIoLEAST16 "o"
    #define PRIxLEAST16 "x"
    #define PRIXLEAST16 "X"

#elif ULONG_MAX >= 65535u

    typedef unsigned long int uint_least16_t;

    #define UINT_LEAST16_MAX ULONG_MAX

    #define UINT_LEAST16_C(x) ((uint_least16_t)x ## LU)

    #define PRIdLEAST16 "ld"
    #define PRIiLEAST16 "li"
    #define PRIuLEAST16 "lu"
    #define PRIoLEAST16 "lo"
    #define PRIxLEAST16 "lx"
    #define PRIXLEAST16 "lX"

#elif defined(ULLONG_MAX) && ULLONG_MAX == 65535u

    typedef unsigned long long int uint_least16_t;

    #define UINT_LEAST16_MAX ULLONG_MAX

    #define UINT_LEAST16_C(x) ((uint_least16_t)x ## LLU)

    #define PRIdLEAST16 "lld"
    #define PRIiLEAST16 "lli"
    #define PRIuLEAST16 "llu"
    #define PRIoLEAST16 "llo"
    #define PRIxLEAST16 "llx"
    #define PRIXLEAST16 "llX"

#endif

/* int_least32_t */

#if SCHAR_MIN <= -2147483648 && SCHAR_MAX >= 2147483647

    typedef signed char int_least32_t;

    #define INT_LEAST32_MIN SCHAR_MIN
    #define INT_LEAST32_MAX SCHAR_MAX

    #define INT_LEAST32_C(x) ((int_least32_t)x)

    #define PRIdLEAST32 "hd"
    #define PRIiLEAST32 "hi"
    #define PRIuLEAST32 "hu"
    #define PRIoLEAST32 "ho"
    #define PRIxLEAST32 "hx"
    #define PRIXLEAST32 "hX"

#elif SHRT_MIN <= -2147483648 && SHRT_MAX >= 2147483647

    typedef signed short int int_least32_t;

    #define INT_LEAST32_MIN SHRT_MIN
    #define INT_LEAST32_MAX SHRT_MAX

    #define INT_LEAST32_C(x) ((int_least32_t)x)

    #define PRIdLEAST32 "hd"
    #define PRIiLEAST32 "hi"
    #define PRIuLEAST32 "hu"
    #define PRIoLEAST32 "ho"
    #define PRIxLEAST32 "hx"
    #define PRIXLEAST32 "hX"

#elif INT_MIN <= -2147483648 && INT_MAX >= 2147483647

    typedef signed int int_least32_t;

    #define INT_LEAST32_MIN INT_MIN
    #define INT_LEAST32_MAX INT_MAX

    #define INT_LEAST32_C(x) ((int_least32_t)x)

    #define PRIdLEAST32 "d"
    #define PRIiLEAST32 "i"
    #define PRIuLEAST32 "u"
    #define PRIoLEAST32 "o"
    #define PRIxLEAST32 "x"
    #define PRIXLEAST32 "X"

#elif LONG_MIN <= -2147483648 && LONG_MAX >= 2147483647

    typedef signed long int int_least32_t;

    #define INT_LEAST32_MIN LONG_MIN
    #define INT_LEAST32_MAX LONG_MAX

    #define INT_LEAST32_C(x) ((int_least32_t)x ## L)

    #define PRIdLEAST32 "ld"
    #define PRIiLEAST32 "li"
    #define PRIuLEAST32 "lu"
    #define PRIoLEAST32 "lo"
    #define PRIxLEAST32 "lx"
    #define PRIXLEAST32 "lX"

#elif defined(LLONG_MIN) && LLONG_MIN <= -2147483648 && defined(LLONG_MAX) && LLONG_MAX >= 2147483647

    typedef signed long long int int_least32_t;

    #define INT_LEAST32_MIN LLONG_MIN
    #define INT_LEAST32_MAX LLONG_MAX

    #define INT_LEAST32_C(x) ((int_least32_t)x ## LL)

    #define PRIdLEAST32 "lld"
    #define PRIiLEAST32 "lli"
    #define PRIuLEAST32 "llu"
    #define PRIoLEAST32 "llo"
    #define PRIxLEAST32 "llx"
    #define PRIXLEAST32 "llX"

#endif

/* uint_least32_t */

#if UCHAR_MAX >= 4294967295u

    typedef unsigned char uint_least32_t;

    #define UINT_LEAST32_MAX UCHAR_MAX

    #define UINT_LEAST32_C(x) ((uint_least32_t)x ## U)

    #define PRIdLEAST32 "hd"
    #define PRIiLEAST32 "hi"
    #define PRIuLEAST32 "hu"
    #define PRIoLEAST32 "ho"
    #define PRIxLEAST32 "hx"
    #define PRIXLEAST32 "hX"

#elif USHRT_MAX >= 4294967295u

    typedef unsigned short int uint_least32_t;

    #define UINT_LEAST32_MAX USHRT_MAX

    #define UINT_LEAST32_C(x) ((uint_least32_t)x ## U)

    #define PRIdLEAST32 "hd"
    #define PRIiLEAST32 "hi"
    #define PRIuLEAST32 "hu"
    #define PRIoLEAST32 "ho"
    #define PRIxLEAST32 "hx"
    #define PRIXLEAST32 "hX"

#elif UINT_MAX >= 4294967295u

    typedef unsigned int uint_least32_t;

    #define UINT_LEAST32_MAX UINT_MAX

    #define UINT_LEAST32_C(x) ((uint_least32_t)x ## U)

    #define PRIdLEAST32 "d"
    #define PRIiLEAST32 "i"
    #define PRIuLEAST32 "u"
    #define PRIoLEAST32 "o"
    #define PRIxLEAST32 "x"
    #define PRIXLEAST32 "X"

#elif ULONG_MAX >= 4294967295u

    typedef unsigned long int uint_least32_t;

    #define UINT_LEAST32_MAX ULONG_MAX

    #define UINT_LEAST32_C(x) ((uint_least32_t)x ## LU)

    #define PRIdLEAST32 "ld"
    #define PRIiLEAST32 "li"
    #define PRIuLEAST32 "lu"
    #define PRIoLEAST32 "lo"
    #define PRIxLEAST32 "lx"
    #define PRIXLEAST32 "lX"

#elif defined(ULLONG_MAX) && ULLONG_MAX >= 4294967295u

    typedef unsigned long long int uint_least32_t;

    #define UINT_LEAST32_MAX ULLONG_MAX

    #define UINT_LEAST32_C(x) ((uint_least32_t)x ## LLU)

    #define PRIdLEAST32 "lld"
    #define PRIiLEAST32 "lli"
    #define PRIuLEAST32 "llu"
    #define PRIoLEAST32 "llo"
    #define PRIxLEAST32 "llx"
    #define PRIXLEAST32 "llX"

#endif

/* int_least64_t */

#if SCHAR_MIN <= -9223372036854775808 && SCHAR_MAX >= 9223372036854775807

    typedef signed char int_least64_t;

    #define INT_LEAST64_MIN SCHAR_MIN
    #define INT_LEAST64_MAX SCHAR_MAX

    #define INT_LEAST64_C(x) ((int_least64_t)x)

    #define PRIdLEAST64 "hd"
    #define PRIiLEAST64 "hi"
    #define PRIuLEAST64 "hu"
    #define PRIoLEAST64 "ho"
    #define PRIxLEAST64 "hx"
    #define PRIXLEAST64 "hX"

#elif SHRT_MIN <= -9223372036854775808 && SHRT_MAX >= 9223372036854775807

    typedef signed short int int_least64_t;

    #define INT_LEAST64_MIN SHRT_MIN
    #define INT_LEAST64_MAX SHRT_MAX

    #define INT_LEAST64_C(x) ((int_least64_t)x)

    #define PRIdLEAST64 "hd"
    #define PRIiLEAST64 "hi"
    #define PRIuLEAST64 "hu"
    #define PRIoLEAST64 "ho"
    #define PRIxLEAST64 "hx"
    #define PRIXLEAST64 "hX"

#elif INT_MIN <= -9223372036854775808 && INT_MAX >= 9223372036854775807

    typedef signed int int_least64_t;

    #define INT_LEAST64_MIN INT_MIN
    #define INT_LEAST64_MAX INT_MAX

    #define INT_LEAST64_C(x) ((int_least64_t)x)

    #define PRIdLEAST64 "d"
    #define PRIiLEAST64 "i"
    #define PRIuLEAST64 "u"
    #define PRIoLEAST64 "o"
    #define PRIxLEAST64 "x"
    #define PRIXLEAST64 "X"

#elif LONG_MIN <= -9223372036854775808 && LONG_MAX >= 9223372036854775807

    typedef signed long int int_least64_t;

    #define INT_LEAST64_MIN LONG_MIN
    #define INT_LEAST64_MAX LONG_MAX

    #define INT_LEAST64_C(x) ((int_least64_t)x ## L)

    #define PRIdLEAST64 "ld"
    #define PRIiLEAST64 "li"
    #define PRIuLEAST64 "lu"
    #define PRIoLEAST64 "lo"
    #define PRIxLEAST64 "lx"
    #define PRIXLEAST64 "lX"

#elif defined(LLONG_MIN) && LLONG_MIN <= -9223372036854775808 && defined(LLONG_MAX) && LLONG_MAX >= 9223372036854775807

    typedef signed long long int int_least64_t;

    #define INT_LEAST64_MIN LLONG_MIN
    #define INT_LEAST64_MAX LLONG_MAX

    #define INT_LEAST64_C(x) ((int_least64_t)x ## LL)

    #define PRIdLEAST64 "lld"
    #define PRIiLEAST64 "lli"
    #define PRIuLEAST64 "llu"
    #define PRIoLEAST64 "llo"
    #define PRIxLEAST64 "llx"
    #define PRIXLEAST64 "llX"

#endif

/* uint_least64_t */

#if UCHAR_MAX >= 18446744073709551615u

    typedef unsigned char uint_least64_t;

    #define UINT_LEAST64_MAX UCHAR_MAX

    #define UINT_LEAST64_C(x) ((uint_least64_t)x ## U)

    #define PRIdLEAST64 "hd"
    #define PRIiLEAST64 "hi"
    #define PRIuLEAST64 "hu"
    #define PRIoLEAST64 "ho"
    #define PRIxLEAST64 "hx"
    #define PRIXLEAST64 "hX"

#elif USHRT_MAX >= 18446744073709551615u

    typedef unsigned short int uint_least64_t;

    #define UINT_LEAST64_MAX USHRT_MAX

    #define UINT_LEAST64_C(x) ((uint_least64_t)x ## U)

    #define PRIdLEAST64 "hd"
    #define PRIiLEAST64 "hi"
    #define PRIuLEAST64 "hu"
    #define PRIoLEAST64 "ho"
    #define PRIxLEAST64 "hx"
    #define PRIXLEAST64 "hX"

#elif UINT_MAX >= 18446744073709551615u

    typedef unsigned int uint_least64_t;

    #define UINT_LEAST64_MAX UINT_MAX

    #define UINT_LEAST64_C(x) ((uint_least64_t)x ## U)

    #define PRIdLEAST64 "d"
    #define PRIiLEAST64 "i"
    #define PRIuLEAST64 "u"
    #define PRIoLEAST64 "o"
    #define PRIxLEAST64 "x"
    #define PRIXLEAST64 "X"

#elif ULONG_MAX >= 18446744073709551615u

    typedef unsigned long int uint_least64_t;

    #define UINT_LEAST64_MAX ULONG_MAX

    #define UINT_LEAST64_C(x) ((uint_least64_t)x ## LU)

    #define PRIdLEAST64 "ld"
    #define PRIiLEAST64 "li"
    #define PRIuLEAST64 "lu"
    #define PRIoLEAST64 "lo"
    #define PRIxLEAST64 "lx"
    #define PRIXLEAST64 "lX"

#elif defined(ULLONG_MAX) && ULLONG_MAX >= 18446744073709551615u

    typedef unsigned long long int uint_least64_t;

    #define UINT_LEAST64_MAX ULLONG_MAX

    #define UINT_LEAST64_C(x) ((uint_least64_t)x ## LLU)

    #define PRIdLEAST64 "lld"
    #define PRIiLEAST64 "lli"
    #define PRIuLEAST64 "llu"
    #define PRIoLEAST64 "llo"
    #define PRIxLEAST64 "llx"
    #define PRIXLEAST64 "llX"

#endif

/* "Fast" integers with at least the whole range of the fixed-width ones. */

/* int_fast8_t */

#if SCHAR_MIN <= -128 && SCHAR_MAX >= 127

    typedef signed char int_fast8_t;

    #define INT_FAST8_MIN SCHAR_MIN
    #define INT_FAST8_MAX SCHAR_MAX

    #define INT_FAST8_C(x) ((int_fast8_t)x)

    #define PRIdFAST8 "hd"
    #define PRIiFAST8 "hi"
    #define PRIuFAST8 "hu"
    #define PRIoFAST8 "ho"
    #define PRIxFAST8 "hx"
    #define PRIXFAST8 "hX"

#elif SHRT_MIN <= -128 && SHRT_MAX >= 127

    typedef signed short int int_fast8_t;

    #define INT_FAST8_MIN SHRT_MIN
    #define INT_FAST8_MAX SHRT_MAX

    #define INT_FAST8_C(x) ((int_fast8_t)x)

    #define PRIdFAST8 "hd"
    #define PRIiFAST8 "hi"
    #define PRIuFAST8 "hu"
    #define PRIoFAST8 "ho"
    #define PRIxFAST8 "hx"
    #define PRIXFAST8 "hX"

#elif INT_MIN <= -128 && INT_MAX >= 127

    typedef signed int int_fast8_t;

    #define INT_FAST8_MIN INT_MIN
    #define INT_FAST8_MAX INT_MAX

    #define INT_FAST8_C(x) ((int_fast8_t)x)

    #define PRIdFAST8 "d"
    #define PRIiFAST8 "i"
    #define PRIuFAST8 "u"
    #define PRIoFAST8 "o"
    #define PRIxFAST8 "x"
    #define PRIXFAST8 "X"

#elif LONG_MIN <= -128 && LONG_MAX >= 127

    typedef signed long int int_fast8_t;

    #define INT_FAST8_MIN LONG_MIN
    #define INT_FAST8_MAX LONG_MAX

    #define INT_FAST8_C(x) ((int_fast8_t)x ## L)

    #define PRIdFAST8 "ld"
    #define PRIiFAST8 "li"
    #define PRIuFAST8 "lu"
    #define PRIoFAST8 "lo"
    #define PRIxFAST8 "lx"
    #define PRIXFAST8 "lX"

#elif defined(LLONG_MIN) && defined(LLONG_MAX) && LLONG_MIN <= -128 && LLONG_MAX >= 127

    typedef signed long long int int_fast8_t;

    #define INT_FAST8_MIN LLONG_MIN
    #define INT_FAST8_MAX LLONG_MAX

    #define INT_FAST8_C(x) ((int_fast8_t)x ## LL)

    #define PRIdFAST8 "lld"
    #define PRIiFAST8 "lli"
    #define PRIuFAST8 "llu"
    #define PRIoFAST8 "llo"
    #define PRIxFAST8 "llx"
    #define PRIXFAST8 "llX"

#endif

/* uint_fast8_t */

#if UCHAR_MAX >= 255u

    typedef unsigned char uint_fast8_t;

    #define UINT_FAST8_MAX UCHAR_MAX

    #define UINT_FAST8_C(x) ((uint_fast8_t)x ## U)

    #define PRIdFAST8 "hd"
    #define PRIiFAST8 "hi"
    #define PRIuFAST8 "hu"
    #define PRIoFAST8 "ho"
    #define PRIxFAST8 "hx"
    #define PRIXFAST8 "hX"

#elif USHRT_MAX >= 255u

    typedef unsigned short int uint_fast8_t;

    #define UINT_FAST8_MAX USHRT_MAX

    #define UINT_FAST8_C(x) ((uint_fast8_t)x ## U)

    #define PRIdFAST8 "hd"
    #define PRIiFAST8 "hi"
    #define PRIuFAST8 "hu"
    #define PRIoFAST8 "ho"
    #define PRIxFAST8 "hx"
    #define PRIXFAST8 "hX"

#elif UINT_MAX >= 255u

    typedef unsigned int uint_fast8_t;

    #define UINT_FAST8_MAX UINT_MAX

    #define UINT_FAST8_C(x) ((uint_fast8_t)x ## U)

    #define PRIdFAST8 "d"
    #define PRIiFAST8 "i"
    #define PRIuFAST8 "u"
    #define PRIoFAST8 "o"
    #define PRIxFAST8 "x"
    #define PRIXFAST8 "X"

#elif LONG_MAX >= 255u

    typedef unsigned long int uint_fast8_t;

    #define UINT_FAST8_MAX LONG_MAX

    #define UINT_FAST8_C(x) ((uint_fast8_t)x ## LU)

    #define PRIdFAST8 "ld"
    #define PRIiFAST8 "li"
    #define PRIuFAST8 "lu"
    #define PRIoFAST8 "lo"
    #define PRIxFAST8 "lx"
    #define PRIXFAST8 "lX"

#elif defined(LLONG_MAX) && LLONG_MAX >= 255u

    typedef unsigned long long int uint_fast8_t;

    #define UINT_FAST8_MAX LLONG_MAX

    #define UINT_FAST8_C(x) ((uint_fast8_t)x ## LLU)

    #define PRIdFAST8 "lld"
    #define PRIiFAST8 "lli"
    #define PRIuFAST8 "llu"
    #define PRIoFAST8 "llo"
    #define PRIxFAST8 "llx"
    #define PRIXFAST8 "llX"

#endif

/* int_fast16_t */

#if INT_MIN <= -32768 && INT_MAX >= 32767

    typedef signed int int_fast16_t;

    #define INT_FAST16_MIN INT_MIN
    #define INT_FAST16_MAX INT_MAX

    #define INT_FAST16_C(x) ((int_fast16_t)x)

    #define PRIdFAST16 "d"
    #define PRIiFAST16 "i"
    #define PRIuFAST16 "u"
    #define PRIoFAST16 "o"
    #define PRIxFAST16 "x"
    #define PRIXFAST16 "X"

#elif LONG_MIN <= -32768 && LONG_MAX >= 32767

    typedef signed long int int_fast16_t;

    #define INT_FAST16_MIN LONG_MIN
    #define INT_FAST16_MAX LONG_MAX

    #define INT_FAST16_C(x) ((int_fast16_t)x ## L)

    #define PRIdFAST16 "ld"
    #define PRIiFAST16 "li"
    #define PRIuFAST16 "lu"
    #define PRIoFAST16 "lo"
    #define PRIxFAST16 "lx"
    #define PRIXFAST16 "lX"

#elif defined(LLONG_MIN) && LLONG_MIN <= -32768 && defined(LLONG_MAX) && LLONG_MAX >= 32767

    typedef signed long long int int_fast16_t;

    #define INT_FAST16_MIN LLONG_MIN
    #define INT_FAST16_MAX LLONG_MAX

    #define INT_FAST16_C(x) ((int_fast16_t)x ## LL)

    #define PRIdFAST16 "lld"
    #define PRIiFAST16 "lli"
    #define PRIuFAST16 "llu"
    #define PRIoFAST16 "llo"
    #define PRIxFAST16 "llx"
    #define PRIXFAST16 "llX"

#endif

/* uint_fast16_t */

#if UINT_MAX >= 65535u

    typedef unsigned int uint_fast16_t;

    #define UINT_FAST16_MAX UINT_MAX

    #define UINT_FAST16_C(x) ((uint_fast16_t)x ## U)

    #define PRIdFAST16 "d"
    #define PRIiFAST16 "i"
    #define PRIuFAST16 "u"
    #define PRIoFAST16 "o"
    #define PRIxFAST16 "x"
    #define PRIXFAST16 "X"

#elif ULONG_MAX >= 65535u

    typedef unsigned long int uint_fast16_t;

    #define UINT_FAST16_MAX ULONG_MAX

    #define UINT_FAST16_C(x) ((uint_fast16_t)x ## LU)

    #define PRIdFAST16 "ld"
    #define PRIiFAST16 "li"
    #define PRIuFAST16 "lu"
    #define PRIoFAST16 "lo"
    #define PRIxFAST16 "lx"
    #define PRIXFAST16 "lX"

#elif defined(ULLONG_MAX) && ULLONG_MAX == 65535u

    typedef unsigned long long int uint_fast16_t;

    #define UINT_FAST16_MAX ULLONG_MAX

    #define UINT_FAST16_C(x) ((uint_fast16_t)x ## LLU)

    #define PRIdFAST16 "lld"
    #define PRIiFAST16 "lli"
    #define PRIuFAST16 "llu"
    #define PRIoFAST16 "llo"
    #define PRIxFAST16 "llx"
    #define PRIXFAST16 "llX"

#endif

/* int_fast32_t */

#if INT_MIN <= -2147483648 && INT_MAX >= 2147483647

    typedef signed int int_fast32_t;

    #define INT_FAST32_MIN INT_MIN
    #define INT_FAST32_MAX INT_MAX

    #define INT_FAST32_C(x) ((int_fast32_t)x)

    #define PRIdFAST32 "d"
    #define PRIiFAST32 "i"
    #define PRIuFAST32 "u"
    #define PRIoFAST32 "o"
    #define PRIxFAST32 "x"
    #define PRIXFAST32 "X"

#elif LONG_MIN <= -2147483648 && LONG_MAX >= 2147483647

    typedef signed long int int_fast32_t;

    #define INT_FAST32_MIN LONG_MIN
    #define INT_FAST32_MAX LONG_MAX

    #define INT_FAST32_C(x) ((int_fast32_t)x ## L)

    #define PRIdFAST32 "ld"
    #define PRIiFAST32 "li"
    #define PRIuFAST32 "lu"
    #define PRIoFAST32 "lo"
    #define PRIxFAST32 "lx"
    #define PRIXFAST32 "lX"

#elif defined(LLONG_MIN) && LLONG_MIN <= -2147483648 && defined(LLONG_MAX) && LLONG_MAX >= 2147483647

    typedef signed long long int int_fast32_t;

    #define INT_FAST32_MIN LLONG_MIN
    #define INT_FAST32_MAX LLONG_MAX

    #define INT_FAST32_C(x) ((int_fast32_t)x ## LL)

    #define PRIdFAST32 "lld"
    #define PRIiFAST32 "lli"
    #define PRIuFAST32 "llu"
    #define PRIoFAST32 "llo"
    #define PRIxFAST32 "llx"
    #define PRIXFAST32 "llX"

#endif

/* uint_fast32_t */

#if UINT_MAX >= 4294967295u

    typedef unsigned int uint_fast32_t;

    #define UINT_FAST32_MAX UINT_MAX

    #define UINT_FAST32_C(x) ((uint_fast32_t)x ## U)

    #define PRIdFAST32 "d"
    #define PRIiFAST32 "i"
    #define PRIuFAST32 "u"
    #define PRIoFAST32 "o"
    #define PRIxFAST32 "x"
    #define PRIXFAST32 "X"

#elif ULONG_MAX >= 4294967295u

    typedef unsigned long int uint_fast32_t;

    #define UINT_FAST32_MAX ULONG_MAX

    #define UINT_FAST32_C(x) ((uint_fast32_t)x ## LU)

    #define PRIdFAST32 "ld"
    #define PRIiFAST32 "li"
    #define PRIuFAST32 "lu"
    #define PRIoFAST32 "lo"
    #define PRIxFAST32 "lx"
    #define PRIXFAST32 "lX"

#elif defined(ULLONG_MAX) && ULLONG_MAX >= 4294967295u

    typedef unsigned long long int uint_fast32_t;

    #define UINT_FAST32_MAX ULLONG_MAX

    #define UINT_FAST32_C(x) ((uint_fast32_t)x ## LLU)

    #define PRIdFAST32 "lld"
    #define PRIiFAST32 "lli"
    #define PRIuFAST32 "llu"
    #define PRIoFAST32 "llo"
    #define PRIxFAST32 "llx"
    #define PRIXFAST32 "llX"

#endif

/* int_fast64_t */

#if INT_MIN <= -9223372036854775808 && INT_MAX >= 9223372036854775807

    typedef signed int int_fast64_t;

    #define INT_FAST64_MIN INT_MIN
    #define INT_FAST64_MAX INT_MAX

    #define INT_FAST64_C(x) ((int_fast64_t)x)

    #define PRIdFAST64 "d"
    #define PRIiFAST64 "i"
    #define PRIuFAST64 "u"
    #define PRIoFAST64 "o"
    #define PRIxFAST64 "x"
    #define PRIXFAST64 "X"

#elif LONG_MIN <= -9223372036854775808 && LONG_MAX >= 9223372036854775807

    typedef signed long int int_fast64_t;

    #define INT_FAST64_MIN LONG_MIN
    #define INT_FAST64_MAX LONG_MAX

    #define INT_FAST64_C(x) ((int_fast64_t)x ## L)

    #define PRIdFAST64 "ld"
    #define PRIiFAST64 "li"
    #define PRIuFAST64 "lu"
    #define PRIoFAST64 "lo"
    #define PRIxFAST64 "lx"
    #define PRIXFAST64 "lX"

#elif defined(LLONG_MIN) && LLONG_MIN <= -9223372036854775808 && defined(LLONG_MAX) && LLONG_MAX >= 9223372036854775807

    typedef signed long long int int_fast64_t;

    #define INT_FAST64_MIN LLONG_MIN
    #define INT_FAST64_MAX LLONG_MAX

    #define INT_FAST64_C(x) ((int_fast64_t)x ## LL)

    #define PRIdFAST64 "lld"
    #define PRIiFAST64 "lli"
    #define PRIuFAST64 "llu"
    #define PRIoFAST64 "llo"
    #define PRIxFAST64 "llx"
    #define PRIXFAST64 "llX"

#endif

/* uint_fast64_t */

#if UINT_MAX >= 18446744073709551615u

    typedef unsigned int uint_fast64_t;

    #define UINT_FAST64_MAX UINT_MAX

    #define UINT_FAST64_C(x) ((uint_fast64_t)x ## U)

    #define PRIdFAST64 "d"
    #define PRIiFAST64 "i"
    #define PRIuFAST64 "u"
    #define PRIoFAST64 "o"
    #define PRIxFAST64 "x"
    #define PRIXFAST64 "X"

#elif ULONG_MAX >= 18446744073709551615u

    typedef unsigned long int uint_fast64_t;

    #define UINT_FAST64_MAX ULONG_MAX

    #define UINT_FAST64_C(x) ((uint_fast64_t)x ## LU)

    #define PRIdFAST64 "ld"
    #define PRIiFAST64 "li"
    #define PRIuFAST64 "lu"
    #define PRIoFAST64 "lo"
    #define PRIxFAST64 "lx"
    #define PRIXFAST64 "lX"

#elif defined(ULLONG_MAX) && ULLONG_MAX >= 18446744073709551615u

    typedef unsigned long long int uint_fast64_t;

    #define UINT_FAST64_MAX ULLONG_MAX

    #define UINT_FAST64_C(x) ((uint_fast64_t)x ## LLU)

    #define PRIdFAST64 "lld"
    #define PRIiFAST64 "lli"
    #define PRIuFAST64 "llu"
    #define PRIoFAST64 "llo"
    #define PRIxFAST64 "llx"
    #define PRIXFAST64 "llX"

#endif

#endif /*C99ZER_STDINT_H*/
