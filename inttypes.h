#ifndef C99ZER_INTTYPES_H
#define C99ZER_INTTYPES_H

/*
    This file is part of C99zer.
    Copyright © 2017 Florin Crișan
    Distributed under the MIT license <https://opensource.org/licenses/MIT>
*/

/* This file should contain definitions for PRId32 and such. It was easier for me to just define them in stdint.h, along with their respective types. */

#include <stdint.h>

#endif /*C99ZER_INTTYPES_H*/
