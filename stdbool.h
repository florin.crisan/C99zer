#ifndef C99ZER_STDBOOL_H
#define C99ZER_STDBOOL_H

/*
    This file is part of C99zer.
    Copyright © 2017 Florin Crișan
    Distributed under the MIT license <https://opensource.org/licenses/MIT>
*/

#ifndef __cplusplus

typedef unsigned char _Bool;

#define bool _Bool
#define true 1
#define false 0

#endif

#define __bool_true_false_are_defined 1

#endif /*C99ZER_STDBOOL_H*/
