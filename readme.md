# C99zer - Retrofitting certain C99 (and later) headers to C89

Implementation for certain C99 (and later) library headers missing from (but easy to add to) a C89 compiler:

 Header | Contents
 ------ | ----
 stdbool.h | `bool` type
 stdint.h | Fixed-width integers
 inttypes.h | Macro constants for using fixed-width integers with `printf`
 wchar.h | Empty header file, still useful to avoid compile errors. Use `#ifdef WCHAR_MAX` to check if `wchar_t` is actually available.

Obviously, this will not give you new language features, such as `long long`, on a compiler that doesn't support it. But it should make it slightly easier to use modern-ish C code with old compilers.

The choices I've made here are fairly generic. There may be better ones for your compiler. Feel free to edit accordingly.

## To use

 1. Copy the header files missing from your compiler's standard library implementation to a C99zer directory somewhere in your file system.
 1. Add the C99zer directory to your compiler's include path.
 1. Start using the new headers.

## License

Distributed under the [MIT license](https://opensource.org/licenses/MIT).

Copyright © 2017 Florin Crișan
