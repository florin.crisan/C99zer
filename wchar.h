#ifndef C99ZER_WCHAR_H
#define C99ZER_WCHAR_H

/*
    This file is part of C99zer.
    Copyright © 2017 Florin Crișan
    Distributed under the MIT license <https://opensource.org/licenses/MIT>
*/

/* This will not add wchar_t-related functionality to a compiler that doesn't support it. You'd need a wrapper over a Unicode library for that to work. This is useful just to avoid compile errors. To check whether wchar_t is actually available, use #ifdef WCHAR_MAX (or something similar). */

#endif /*C99ZER_WCHAR_H*/
